*******************************************************
    README.txt for Open Graph Comments module
*******************************************************

Contents:
=========
1. ABOUT
2. REQUIREMENTS
3. INSTALLATION

1. ABOUT
===========

Ideate is a modern, [Sass](http://sass-lang.com/) and
[Compass](http://compass-style.org/) enabled Drupal 7 base theme.
2. INSTALLATION
================

In order to install Ideate, simply place it in your sites theme folder
(normally located at sites/all/themes). Ideate is a base theme and as such
should **never** be used or modified directly.

3. DOCUMENTATION
================
The [Ideate documentation](https://www.drupal.org/node/2310979) is hosted on
[drupal.org](http://drupal.org) as part of the handbook section. Please note
that we are still in the process of writing the documentation. If you want to
get involved, please contact us.

4. MAINTAINANCE
================
This version of the Ideate theme is maintained by
[Manvendra Singh Rawat](https://www.drupal.org/node/2310979)