<?php

/**
 * Ideate's theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 */
?>

  <div id="wrapperCntr">
  
    <div id="mainCntr" class="clearfix page-row page-row-expanded">
        
        <header id="header"  class="header  clearfix" role="banner">
        
            <div class="wrapper">
            
                <div id="logo_floater">
                <?php if ($logo || $site_title): ?>
                <?php if ($title): ?>
                <div id="branding"><a href="<?php print $front_page ?>">
                    <?php if ($logo): ?>
                    <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logo" />
                    <?php endif; ?>
                    <?php print $site_html ?>
                </a></div>
                <?php else: /* Use h1 when the content title is empty */ ?>
                <h1 id="branding">
                    <a href="<?php print $front_page ?>">
                        <?php if ($logo): ?>
                        <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logo" />
                        <?php endif; ?>
                        <?php print $site_html ?>
                    </a>
                </h1>
                <?php endif; ?>
                <?php endif; ?>
            
            </div>
        
                <?php if ($main_menu): ?>
                  <nav id="main-menu" role="navigation" class="navigation">
                    <?php print theme('links__system_main_menu', array(
                      'links' => $main_menu,
                      'attributes' => array(
                        'id' => 'main-menu-links',
                        'class' => array('links', 'clearfix','main-menu'),
                      ),
                      'heading' => array(
                        'text' => t('Main menu'),
                        'level' => 'h2',
                        'class' => array('element-invisible'),
                      ),
                    )); ?>
                  </nav> <!-- /#main-menu -->
                <?php endif; ?>
                
                <?php if ($secondary_nav): print $secondary_nav; endif; ?>
                
            </div>
                
			<?php if($page['header']):  ?>
            <div class="header_bottom">
            
                <div class="wrapper">
                
                    <?php print render($page['header']); ?>
                    
                </div>
            
            </div>
            
            <?php endif; ?>
            
        </header> <!-- /#header -->
      
        <div id="navigation" class="clearfix">
        
        	<div class="wrapper">
        
				<?php print render($page['navigation']); ?>
            
            </div>
            
        </div>
        
        <div id="contentCntr">
        
        <div class="wrapper">
        
          <?php print $breadcrumb; ?>
        
          <div id="center"  role="main" >
              <div id="squeeze">
                  
                  <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
                  <a id="main-content"></a>
                  <?php if ($tabs): ?><div id="tabs-wrapper" class="clearfix"><?php endif; ?>
                  <?php print render($title_prefix); ?>
                  <?php if ($title): ?>
                    <h1<?php print $tabs ? ' class="with-tabs"' : '' ?>><?php print $title ?></h1>
                  <?php endif; ?>
                  <?php print render($title_suffix); ?>
                  <?php if ($tabs): ?><?php print render($tabs); ?></div><?php endif; ?>
                  <?php print render($tabs2); ?>
                  <?php print $messages; ?>
                  <?php print render($page['help']); ?>
                  <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
                  <div class="clearfix">
                    <?php print render($page['content']); ?>
                  </div>
                  <?php print $feed_icons ?>
                  
              </div>
          </div> <!-- /.left-corner, /.right-corner, /#squeeze, /#center -->
          
          <?php if ($page['sidebar_first']): ?>
            <aside id="sidebar-first" class="sidebar">
              <?php print render($page['sidebar_first']); ?>
            </aside>
          <?php endif; ?>
    
          <?php if ($page['sidebar_second']): ?>
            <aside id="sidebar-second" class="sidebar">
              <?php print render($page['sidebar_second']); ?>
            </aside>
          <?php endif; ?>
      
      </div>
      
      </div>

    </div> <!-- /#container -->
    
    <div id="footerCntr" class="page-row clearfix">
    
    	<div class="wrapper">
    
    		<?php print render($page['footer']); ?>
        
        </div>
        
    </div>
    
    <?php print render($page['bottom']); ?>
    
  </div> <!-- /#wrapper Cntr -->
