<?php

/**
 * @file
 * Theme setting callbacks for the ideate theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function ideate_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['ideate_width'] = array(
    '#type' => 'radios',
    '#title' => t('Content width'),
    '#options' => array(
      'fluid' => t('Fluid width'),
      'fixed' => t('Fixed width'),
    ),
    '#default_value' => theme_get_setting('ideate_width'),
    '#description' => t('Specify whether the content will wrap to a fixed width or will fluidly expand to the width of the browser window.'),
    // Place this above the color scheme options.
    '#weight' => -2,
  );
}
